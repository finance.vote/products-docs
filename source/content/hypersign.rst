Hypersign
===========

.. contents::

Merkle Resistor contract
------------------------

.. image:: ../images/merkle-resistor.png
    :align: center
    :alt: merkle resistor
    :width: 75%

1. **Inputs**:

    -	destination - wallet address,
    -	minTotalPayments - minimum total payments in wei
    -	maxTotalPayments - max total payments in wei

| To add new input row use “Add new element button”
| To change basic inputs to advance mode please use “Advance mode” button

2. **Select project** – available: (finance vote, Cudos, Totem, BUMP, Hypersign Identity, Lepricon, Metis, Olyseum

3. **Minimum end time** (offset in seconds): minimum end time, offset in seconds of vesting schedule


4. **Maximum end time** (offset in seconds): max end time, offset in seconds of vesting schedule

5. **Percent up front**: percent of vested coins that will be withdrawn after initialization

6. **Deposit token**: deposit token contract address

If form will be valid please press “Upload to blockchain” button

7. Creating the url with query params to prefill the form:
   
    1.	const part - /#/admin/add-merkle-root
    2.	formType – type of form. In this case resistor
    3.	projectType – type of project. Available project ids:

        -	Finance.vote – fvt
        -	Cudos – cudos
        -	Totem – totm
        -	BUMP – bump
        -	Hypersign Identity – hid
        -	Lepricon – l3p,
        -	Metis – metis,
        -	Olyseum – oly

    4.	minEndTime - minimum end time, offset in seconds of vesting schedule
    5.	maxEndTime - max end time, offset in seconds of vesting schedule
    6.	pctUpFront - percent of vested coins that will be withdrawn after initialization
    7.	depositToken - deposit token contract addresss
    8.	merkleItems – array of objects to inputs as JSON string. More details in first point “Inputs”.


Path Example::

    .../#/admin/add-merkle-root?formType=resistor&projectType=fvt&minEndTime=8000&maxEndTime=8400&pctUpFront=20&depositToken=0xa86256627d66c40163ADb33274610f4b881b0D45&merkleItems=[{"destination":"0x1969d467a6c2999889ddb068c0c4fa0b5e3fe3dc","minTotalPayments":"10000000000","maxTotalPayments":"500000000000"},{"destination":"0x614dc59f2319E4B05002640478f35544bBc0e2Cf","minTotalPayments":"10000000000","maxTotalPayments":"500000000000"},{"destination":"0xe4Cd56f91Bc79cC610AEfB1bE92b07BB5b6F2e30","minTotalPayments":"10000000000","maxTotalPayments":"500000000000"}]



Merkle Drop Factory contract
------------------------------

.. image:: ../images/merkle-drop.png
    :align: center
    :alt: merkle drop
    :width: 75%

1. **Inputs**:

    -	destination - wallet address,
    -	value - max amount of tokens in wei that wallet can receive

| To add new input row please use “Add new element” button
| To change basic inputs to advance mode please use “Advance mode” button

2. **Select project** – available: (finance vote, Cudos, Totem, BUMP, Hypersign Identity, Lepricon, Metis, Olyseum)

3. **Deposit token**: deposit token contract address

| If form will be valid please press “Upload to blockchain” button


4.	Creating the url with query params to prefill the form:

    1.	const part - /#/admin/add-merkle-root
    2.	formType – type of form. In this case drop
    3.	projectType – type of project. Available project ids:

        -	Finance.vote – fvt
        -	Cudos – cudos
        -	Totem – totm
        -	BUMP – bump
        -	Hypersign Identity – hid
        -	Lepricon – l3p,
        -	Metis – id: metis,
        -	Olyseum – id: oly
    4.	depositToken - deposit token contract address
    5.	merkleItems – array of objects to inputs as JSON string. More details in first point “Inputs”.



Path Example::

   ... /#/admin/add-merkle-root?formType=drop&projectType=fvt&depositToken=0xa86256627d66c40163ADb33274610f4b881b0D45&merkleItems=[{"destination":"0xc1a282bce651eeab1a645ee0b9c5fdd2a186be12","value":"50000000000000000000"},{"destination":"0xe223c6bc64cdb30f8004612d84f4f05963f34577","value":"50000000000000000000"},{"destination":"0x64f63cf1905e92715c7d42122eaa3712a9d83fbf","value":"50000000000000000000"},{"destination":"0xde6856ba503eaa0a108fa7ad0514ef527beff700","value":"50000000000000000000"},{"destination":"0x1969d467a6c2999889ddb068c0c4fa0b5e3fe3dc","value":"50000000000000000000"},{"destination":"0x1deD73e0C96Bd1Fe1678503B8cAC8D46F37A299B","value":"50000000000000000000"}]

Merkle Vesting contract
-------------------------

 .. image:: ../images/merkle-vesting.png
    :align: center
    :alt: merkle vesting
    :width: 75%


1. **Inputs**:

    -	destination – wallet address
    -	totalCoins - max amount of tokens in wei that wallet can receive
    -	startTime - timestamp - vesting period start,
    -	endTime - timestamp - vesting period end,
    -	lockPerionEndTime - timestamp - lock period end time

| To add new input row use “Add new element” button
| To change basic inputs to advance mode please use “Advance mode” button

2. **Select project** – available: (finance vote, Cudos, Totem, BUMP, Hypersign Identity, Lepricon, Metis, Olyseum)

3. **Deposit token**: deposit token contract address

| If form will be valid please press “Upload to blockchain” button

4.	Creating the url with query params to prefill the form:

    1.	const part - /#/admin/add-merkle-root
    2.	formType – type of form. In this case vesting
    3.	projectType – type of project. Available project ids:

        -	Finance.vote – fvt
        -	Cudos – cudos
        -	Totem – totm
        -	BUMP – bump
        -	Hypersign Identity – hid
        -	Lepricon – l3p,
        -	Metis – id: metis,
        -	Olyseum – id: oly
    4.	depositToken - deposit token contract address
    5.	merkleItems – array of objects to inputs as JSON string. More details in first point “Inputs”.

Path Example::

    ... /#/admin/add-merkle-root?formType=vesting&projectType=fvt&depositToken=0xa86256627d66c40163ADb33274610f4b881b0D45&merkleItems=[{"destination":"0x3ae257065d690110e28904dbe252b106881b8903","totalCoins":"10000000000000000000000","startTime":1648129851,"endTime":1653400311,"lockPeriodEndTime":1648629851},{"destination":"0x02f599b9e574af582b05c1b5a11446d9bf1e2f70","totalCoins":"10000000000000000000000","startTime":1648129851,"endTime":1653400311,"lockPeriodEndTime":1648629851},{"destination":"0x92e128e0f066a20d2f121ec9753a558ca53c5bf8","totalCoins":"10000000000000000000000","startTime":1648129851,"endTime":1653400311,"lockPeriodEndTime":1648629851},{"destination":"0xa2601d29e7ebb81237b201076851e0611a7c2572","totalCoins":"10000000000000000000000","startTime":1648129851,"endTime":1653400311,"lockPeriodEndTime":1648629851}]
